#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QLogValueAxis>
#include <QtCharts/QValueAxis>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtGui/QImage>
#include <iostream>
#include <chrono>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>
//#include <unsupported/Eigen/IterativeSolvers>
#include <fftw3.h>



QT_CHARTS_USE_NAMESPACE

void saveAsBitmap(const Eigen::VectorXd & a, int n, const char * filename)
{
    Eigen::VectorXd b = a;
    double *x = b.data();
    double max = x[0], min = x[0];
    for (int i = 0; i < n*n; i++) {
        if (x[i] > max) max = x[i];
        if (x[i] < min) min = x[i];
    }
    for (int i = 0; i < n*n; i++) {
        x[i] = (x[i] - min)/(max - min);
    }
    QImage img(n, n, QImage::Format_RGB32);
    for (int j = 0; j < n; j++) {
      for (int i = 0; i < n; i++) {
        int id = i+j*n;
        unsigned char c;
        c = x[id]*255;
        img.setPixel(i, j, qRgb(0,c,c));
     }
    }
    img.save(filename);
    //img.mirrored(false,true).save(filename);
}


using namespace std::chrono;
class MatrixReplacement;
using Eigen::SparseMatrix;

namespace Eigen {
namespace internal {
  // MatrixReplacement looks-like a SparseMatrix, so let's inherits its traits:
  template<>
  struct traits<MatrixReplacement> :  public Eigen::internal::traits<Eigen::SparseMatrix<double> >
  {};
}
}


// Example of a matrix-free wrapper from a user type to Eigen's compatible type
class MatrixReplacement : public Eigen::EigenBase<MatrixReplacement> {
private:
    int N;
    double h;
public:

  // Required typedefs, constants, and method:
  typedef double Scalar;
  typedef double RealScalar;
  typedef int StorageIndex;
  enum {
    ColsAtCompileTime = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    IsRowMajor = false
  };
  Index rows() const { return N*N; }
  Index cols() const { return N*N; }
  template<typename Rhs>
  Eigen::Product<MatrixReplacement,Rhs,Eigen::AliasFreeProduct> operator*(const Eigen::MatrixBase<Rhs>& x) const {
    return Eigen::Product<MatrixReplacement,Rhs,Eigen::AliasFreeProduct>(*this, x.derived());
  }
  // Custom API:
  //MatrixReplacement() : N(0), h(0) {}
  //MatrixReplacement(int N, double h) : N(N), h(h) {}
  void set_params(int NN, double hh)
  {
      N = NN;
      h = hh;
  }
  double alphad(int i, int j) const
  {
      return  -cos(i*h)/(2*h*h);
  }
  int get_N() const
  {
      return N;
  }
};

// Implementation of MatrixReplacement * Eigen::DenseVector though a specialization of internal::generic_product_impl:
namespace Eigen {
namespace internal {
  template<typename Rhs>
  struct generic_product_impl<MatrixReplacement, Rhs, SparseShape, DenseShape, GemvProduct> // GEMV stands for matrix-vector
  : generic_product_impl_base<MatrixReplacement,Rhs,generic_product_impl<MatrixReplacement,Rhs> >
  {
    typedef typename Product<MatrixReplacement,Rhs>::Scalar Scalar;
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const MatrixReplacement& lhs, const Rhs& rhs, const Scalar& alpha)
    {
      // This method should implement "dst += alpha * lhs * rhs" inplace,
      // however, for iterative solvers, alpha is always equal to 1, so let's not bother about it.
      assert(alpha==Scalar(1) && "scaling is not implemented");
      EIGEN_ONLY_USED_FOR_DEBUG(alpha);

      //come back to matrix representation
      const int N = lhs.get_N();
      Eigen::Map<const Eigen::MatrixXd> U(rhs.data(), N, N);
      Eigen::Map<Eigen::MatrixXd> D(dst.data(), N, N);
      for (int i = 1; i < N+1; i++) {
          for (int j = 1; j < N+1; j++) {
              D(i-1, j-1) = (lhs.alphad(i,j) + lhs.alphad(i+1,j))
                            * ((i+1 == N+1? 0: U(i, j-1)) - U(i-1,j-1))
                            -
                            (lhs.alphad(i-1,j) + lhs.alphad(i,j))
                            * (U(i-1, j-1) - (i-1 == 0? 0: U(i-2,j-1)))
                            +
                            (lhs.alphad(i, j+1) + lhs.alphad(i, j))
                            * ((j+1 == N+1? 0: U(i-1, j)) - U(i-1, j-1))
                            -
                            (lhs.alphad(i, j) + lhs.alphad(i, j-1))
                            * (U(i-1, j-1) - (j-1 == 0? 0: U(i-1, j-2)));
          }
      }

    }
  };
}
}


class IdentPrecond
{
public:
    IdentPrecond(int N = 0, double h = 0)
    {}
    void precond(const Eigen::VectorXd & from, Eigen::VectorXd & to)
    {
        to =  from;
    }
};
class JacobiPrecond
{
    Eigen::VectorXd c;
    int N;
    double h;
    double alphad(int i, int j) const
    {
        return  -cos(i*h)/(2*h*h);
    }
public:
    JacobiPrecond(int N, double h)
        : c(N*N), N(N), h(h)
    {
        Eigen::Map<Eigen::MatrixXd> cm(c.data(), N, N);
        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < N+1; j++)
                cm(i-1, j-1) = -1.0/(4*alphad(i,j) + alphad(i+1,j) + alphad(i-1,j) + alphad(i, j+1) + alphad(i, j-1));
        }
    }
    void precond(const Eigen::VectorXd & from, Eigen::VectorXd & to)
    {
        to = from.cwiseProduct(c);
    }
};

class GaussSeidelPrecond
{ //M = (D+L^T)^-1 D (D+L)^-1
    int N;
    double h;
    double alphad(int i, int j) const
    {
        return  -cos(i*h)/(2*h*h);
    }

public:
    GaussSeidelPrecond(int N, double h)
        : N(N), h(h)
    {}
    void precond(const Eigen::VectorXd & from, Eigen::VectorXd & to)
    {
        Eigen::Map<const Eigen::MatrixXd> FROM(from.data(), N, N);
        Eigen::Map<Eigen::MatrixXd> TO(to.data(), N, N);
        //solve to = (D+L)^-1 from
        for (int j = 1; j < N+1; j++) {
            for (int i = 1; i < N+1; i++) {
                const double coef = -(4*alphad(i,j) + alphad(i,j+1) + alphad(i,j-1) + alphad(i-1, j) + alphad(i+1, j));
                TO(i - 1, j - 1) = 1.0/coef *
                                   (FROM(i-1, j-1)
                                     - (alphad(i-1,j) + alphad(i,j))*
                                     (i-1 == 0? 0: TO(i-2, j-1))
                                     - (alphad(i,j) + alphad(i,j-1))*
                                     (j-1 == 0? 0: TO(i-1, j-2)));
            }
        }
        //now D
        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < N+1; j++)
                TO(i-1, j-1) *= -1.0/(4*alphad(i,j) + alphad(i+1,j) + alphad(i-1,j) + alphad(i, j+1) + alphad(i, j-1));
        }

        //solve to = (D+L^T)^-1 to
        for (int j = N; j > 0; j--) {
            for (int i = N; i > 0; i--) {
                const double coef = -(4*alphad(i,j) + alphad(i,j+1) + alphad(i,j-1) + alphad(i-1, j) + alphad(i+1, j));
                TO(i - 1, j - 1) = 1.0/coef *
                                   (TO(i-1, j-1)
                                     - (alphad(i+1,j) + alphad(i,j))*
                                     (i+1 == N+1? 0: TO(i, j-1))
                                     - (alphad(i,j+1) + alphad(i,j))*
                                     (j+1 == N+1? 0: TO(i-1, j)));
            }
        }

    }
};

class DSTPrecond
{
    int N;
    double h;
    //double *s;//small buf
    double *b;//big buf
    fftw_plan plan;

public:
    DSTPrecond(int N, double h)
        : N(N), h(h)
    {
      //  s = fftw_alloc_real(N);
        b = fftw_alloc_real(N*N);
    }
    ~DSTPrecond()
    {
        //fftw_free(s);
        fftw_free(b);
    }
    void precond(const Eigen::VectorXd & from, Eigen::VectorXd & to)
    {
        for (int i = 0; i < N*N; i++)
            b[i] = from(i);
/*
        //axis 0
        for (int i = 0 ; i < N; i++) {
            //prepare one vector
            plan = fftw_plan_r2r_1d(N, b+i*N, b+i*N, FFTW_RODFT00, FFTW_ESTIMATE);
            fftw_execute ( plan );
        }
        //axis 1
        for (int i = 0 ; i < N; i++) {
            //prepare one vector
            for (int j = 0; j < N; j++) {
                s[j] = b[i+j*N];
            }
            plan = fftw_plan_r2r_1d(N, s, s, FFTW_RODFT00, FFTW_ESTIMATE);
            fftw_execute ( plan );
            //now save
            for (int j = 0; j < N; j++) {
                b[i+j*N] = s[j];
            }
        }
*/
        plan = fftw_plan_r2r_2d(N, N, b, b, FFTW_RODFT00, FFTW_RODFT00, FFTW_ESTIMATE);
        fftw_execute ( plan );


        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                double d = 4.0 * (pow(sin(M_PI * (i+1) / (2*(N+1) )), 2) + pow(sin(M_PI * (j+1) / (2*(N+1) )), 2)) / (h*h);
                b[i+j*N] /= d;
            }
        }

        //inverse
        //axis 1
        /*
        for (int i = 0 ; i < N; i++) {
             //prepare one vector
             for (int j = 0; j < N; j++) {
                 s[j] = b[i+j*N];
             }
             plan = fftw_plan_r2r_1d(N, s, s, FFTW_RODFT00, FFTW_ESTIMATE);
             fftw_execute ( plan );
             //now save
             for (int j = 0; j < N; j++) {
                 b[i+j*N] = s[j];
             }
        }
        //axis 0
        for (int i = 0 ; i < N; i++) {
            //prepare one vector
            plan = fftw_plan_r2r_1d(N, b+i*N, b+i*N, FFTW_RODFT00, FFTW_ESTIMATE);
            fftw_execute ( plan );
        }
*/
        plan = fftw_plan_r2r_2d(N, N, b, b, FFTW_RODFT00, FFTW_RODFT00, FFTW_ESTIMATE);
        fftw_execute ( plan );

        //save
        for (int i = 0; i < N*N; i++)
            to(i) = b[i] / pow((2*(N+1)), 2) ;//normalize if you wish
    }
};

template <typename Matrix, typename Precond>
void PCG(Eigen::VectorXd & x, const Eigen::VectorXd & b,
         const Matrix & A, Precond & M, QLineSeries & series, int & iters, int kmax = 10000, double eps = 1e-9)
{
    Eigen::VectorXd r = b - A*x;
    double bnorm = b.squaredNorm();
    double rn = r.squaredNorm();
    const int N = A.get_N();
    eps*=eps;
    Eigen::VectorXd z(N*N), p(N*N), w(N*N);
    double t1 = 0, t2 = 0;
    double alpha, beta = 0;
    int i;
    for (i = 0; i < kmax && rn > eps*bnorm; i++) {
        series.append(i, sqrt(rn));
        M.precond(r, z);
        t2 = t1;
        t1 = z.dot(r);
        if (i == 0) {
            p = z;
        } else {
            beta = t1/t2;
            p = z + beta*p;
        }
        w = A*p;
        alpha = t1/p.dot(w);
        x += alpha*p;
        r -= alpha*w;
        rn = r.squaredNorm();
    }
    rn = sqrt(rn);
    iters = i;
    //std::cout << "Iters: " << i << " rsd: " << rn << std::endl;
}


class Task
{
    double alphad(int i, int j) const
    {
        return  -cos(i*h)/(2*h*h);
    }
    MatrixReplacement A;

public:

    int N; //mesh NxN
    double h;
    Eigen::VectorXd b, x;
    double boundary(int i, int j)
    {
        return 0;
    }
    void rhs()
    {
        Eigen::Map<Eigen::MatrixXd> B(b.data(), N, N);

        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < N+1; j++) {
                const double x = i*h;
                const double y = j*h;
                B(i-1, j-1) = - cos(x) * 10 * y*(1-y)*exp(pow(x,4.5)) *
                              (4.5*pow(x,3.5)*(1-2*x+4.5*pow(x,3.5)*(x-x*x)) +
                               (-2+4.5*3.5*pow(x,2.5)*(x-x*x)+4.5*pow(x,3.5)*(1-2*x)))
                              + sin(x)*10*y*(1-y)*exp(pow(x,4.5))*(1-2*x +4.5*pow(x,3.5)*(x-x*x))
                              - cos(x)* (-20)*x*(1-x)*exp(pow(x,4.5));

                B(i-1, j-1) -= (alphad(i,j) + alphad(i+1,j))
                              * (i+1 == N+1? boundary(i+1, j): 0)
                              -
                              (alphad(i-1,j) + alphad(i,j))
                              * (- (i-1 == 0? boundary(i-1, j): 0))
                              +
                              (alphad(i, j+1) + alphad(i, j))
                              * ((j+1 == N+1? boundary(i, j+1): 0))
                              -
                              (alphad(i, j) + alphad(i, j-1))
                              * (- (j-1 == 0? boundary(i, j-1): 0));
            }
        }
    }
    Eigen::VectorXd exact_sol()
    {
        Eigen::VectorXd exact_x(N*N);
        Eigen::Map<Eigen::MatrixXd> E(exact_x.data(), N, N);

        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < N+1; j++) {
                const double x = i*h;
                const double y = j*h;
                E(i-1, j-1) = 10*x*y*(1-x)*(1-y)*exp(pow(x,4.5));
            }
        }
        return exact_x;

    }
    Task(int N)
        : N(N), h(1.0/(N+1)), b(N*N), x(N*N)
    {
        rhs();
        A.set_params(N, h);
    }



    void solve_eigen_cg()
    {
        Eigen::ConjugateGradient<MatrixReplacement, Eigen::Lower|Eigen::Upper, Eigen::IdentityPreconditioner> cg;
        cg.compute(A);
        cg.setTolerance(1e-18);
        x = cg.solve(b);
        //x.setZero();
        //x = cg.solveWithGuess(b,x);

        std::cout << "Eigen_CG:       #iter: " << cg.iterations() << ", estimated error: " << cg.error() << std::endl;

        double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
        std::cout << "Eigen_CG Error: " <<  err << std::endl;

    }


      template<typename Precond>
      void solve_precond(QLineSeries & series, double & time, int & iters)
      {
         Precond M(N, h);
         x.setZero();
         high_resolution_clock::time_point t1 = high_resolution_clock::now();
         PCG(x, b, A, M, series, iters);
         high_resolution_clock::time_point t2 = high_resolution_clock::now();
         time = duration_cast<milliseconds>( t2 - t1 ).count();

         double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
         std::cout << "Error: " <<  err << std::endl;
         //std::cout << "Time (ms): " << time << std::endl;
    }

};


void benchmark()
{
    std::vector<int> N = {32, 64};
    int tn = 5;
    double time;
    int iters;
    for (auto &n : N) {
        Task task(n);
        std::cout << "Task " << n << "x" << n << std::endl;
//Poisson
        time = 0;
        iters = 0;
        for (int i = 0; i < tn; i++) {
            QLineSeries series;
            double tt;
            int iters_t;
            task.solve_precond<DSTPrecond>(series, tt, iters_t);
            time+=tt;
            iters+=iters_t;
        }
        time /= tn;
        iters /= tn;
        std::cout <<  n << "x" << n << " Poisson: " << "t(ms): " << time << " iter: " << iters << std::endl;
//Ident
        time = 0;
        iters = 0;
        for (int i = 0; i < tn; i++) {
            QLineSeries series;
            double tt;
            int iters_t;
            task.solve_precond<IdentPrecond>(series, tt, iters_t);
            time+=tt;
            iters+=iters_t;
        }
        time /= tn;
        iters /= tn;
        std::cout <<  n << "x" << n << " Ident: " << "t(ms): " << time << " iter: " << iters << std::endl;
//Jacobi
        time = 0;
        iters = 0;
        for (int i = 0; i < tn; i++) {
            QLineSeries series;
            double tt;
            int iters_t;
            task.solve_precond<JacobiPrecond>(series, tt, iters_t);
            time+=tt;
            iters+=iters_t;
        }
        time /= tn;
        iters /= tn;
        std::cout <<  n << "x" << n << " Jacobi: " << "t(ms): " << time << " iter: " << iters << std::endl;
//Gauss-Seidel
        time = 0;
        iters = 0;
        for (int i = 0; i < tn; i++) {
            QLineSeries series;
            double tt;
            int iters_t;
            task.solve_precond<GaussSeidelPrecond>(series, tt, iters_t);
            time+=tt;
            iters+=iters_t;
        }
        time /= tn;
        iters /= tn;
        std::cout <<  n << "x" << n << " Gauss-Seidel: " << "t(ms): " << time << " iter: " << iters << std::endl;

    }
}



double boundary_t(int i, int j, double h)
{
    return 0;
}

Eigen::VectorXd exact_sol_t(int N)
{
    Eigen::VectorXd exact_x(N*N);
    Eigen::Map<Eigen::MatrixXd> E(exact_x.data(), N, N);
    const double h = 1.0/(N+1);
    for (int i = 1; i < N+1; i++) {
        for (int j = 1; j < N+1; j++) {
            const double x = i*h;
            const double y = j*h;
            E(i-1, j-1) = -x*(x-1)*y*(y-1)*100;
        }
    }
    return exact_x;
}


void test_fft()
{//2d mesh solve homogeneous dirichlet problem for poisson equation on (0,1)^2
    int N = 32;
    const double h = 1.0/(N+1);
    DSTPrecond directSolver(N, h);
    //set rhs
    Eigen::VectorXd b(N*N), x(N*N);
    Eigen::Map<Eigen::MatrixXd> B(b.data(), N, N);
    const double alpha = -1.0/(h*h);

    for (int i = 1; i < N+1; i++) {
        for (int j = 1; j < N+1; j++) {
            const double x = i*h;
            const double y = j*h;
            B(i-1, j-1) = 2*(x*(x-1) + y*(y-1))*100;


            B(i-1, j-1) -= alpha
                          * (i+1 == N+1? boundary_t(i+1, j, h): 0)
                          -
                          alpha
                          * (- (i-1 == 0? boundary_t(i-1, j, h): 0))
                          +
                          alpha
                          * ((j+1 == N+1? boundary_t(i, j+1, h): 0))
                          -
                          alpha
                          * (- (j-1 == 0? boundary_t(i, j-1, h): 0));
        }
    }

    directSolver.precond(b, x);
    double err = (x - exact_sol_t(N)).lpNorm<Eigen::Infinity>();
    std::cout << "DSTPrecond as direct solver Error: " <<  err << std::endl;
}


int main(int argc, char *argv[])
{
    test_fft();
    return 0;
    benchmark();
    int N = 128;
    Task task(N);
//test with build in cg
    task.solve_eigen_cg();
    QApplication app(argc, argv);
    int iters; //stub;
    double time; //stub;

    QLineSeries *poisson = new QLineSeries();
    poisson->setName("Poisson solver");
    task.solve_precond<DSTPrecond>(*poisson, time, iters);
    saveAsBitmap((task.x - task.exact_sol()).cwiseAbs(), N, "error.bmp");
    saveAsBitmap(task.x, N, "appr_solution.bmp");
    saveAsBitmap(task.exact_sol(), N, "exect_solution.bmp");

    QLineSeries *gaussseidel = new QLineSeries();
    gaussseidel->setName("Gauss-Seidel");
    task.solve_precond<GaussSeidelPrecond>(*gaussseidel, time, iters);

    QLineSeries *jacobi = new QLineSeries();
    jacobi->setName("Jacobi");
    task.solve_precond<JacobiPrecond>(*jacobi, time, iters);

    QLineSeries *ident = new QLineSeries();
    ident->setName("NoPrecond");
    task.solve_precond<IdentPrecond>(*ident, time, iters);


    QChart *chart = new QChart();
    chart->addSeries(poisson);
    chart->addSeries(gaussseidel);
    chart->addSeries(jacobi);
    chart->addSeries(ident);

    //chart->legend()->hide();
    chart->setTitle(QStringLiteral("Preconditioned CG, mesh %1x%1").arg(N) );

    QValueAxis *axisX = new QValueAxis();
    axisX->setTitleText("Iterations");
    axisX->setLabelFormat("%i");
    axisX->setTickCount(ident->count()/poisson->count()*2);
    axisX->setMax(ident->count());
    chart->addAxis(axisX, Qt::AlignBottom);
    poisson->attachAxis(axisX);
    gaussseidel->attachAxis(axisX);
    jacobi->attachAxis(axisX);
    ident->attachAxis(axisX);

    QLogValueAxis *axisY = new QLogValueAxis();
    axisY->setTitleText("Residual");
    axisY->setLabelFormat("%g");
    axisY->setBase(8.0);
    axisY->setMax(1000);
    axisY->setMin(poisson->at(poisson->count()-1).y()/2);
    axisY->setMinorTickCount(-1);
    chart->addAxis(axisY, Qt::AlignLeft);
    poisson->attachAxis(axisY);
    gaussseidel->attachAxis(axisY);
    jacobi->attachAxis(axisY);
    ident->attachAxis(axisY);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QMainWindow window;
    window.setCentralWidget(chartView);
    window.resize(800, 600);
    window.show();

    return app.exec();
}
