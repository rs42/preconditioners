QT += charts
CONFIG += c++11

INCLUDEPATH += /usr/include/eigen3
QMAKE_CXXFLAGS += -O3 -Wall -march=native
QMAKE_LFLAGS +=  -lfftw3 -lm

SOURCES += \
    main.cpp

target.path = $$[QT_INSTALL_EXAMPLES]/charts/logvalueaxis
INSTALLS += target
