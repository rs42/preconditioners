#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QLogValueAxis>
#include <QtCharts/QValueAxis>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtGui/QImage>
#include <iostream>
#include <chrono>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>
#include <unsupported/Eigen/IterativeSolvers>
#include <fftw3.h>
#include <cmath>



QT_CHARTS_USE_NAMESPACE

void saveAsBitmap(const Eigen::VectorXd & a, int n, const char * filename)
{
    Eigen::VectorXd b = a;
    double *x = b.data();
    double max = x[0], min = x[0];
    for (int i = 0; i < n*n; i++) {
        if (x[i] > max) max = x[i];
        if (x[i] < min) min = x[i];
    }
    for (int i = 0; i < n*n; i++) {
        x[i] = (x[i] - min)/(max - min);
    }
    QImage img(n, n, QImage::Format_RGB32);
    for (int j = 0; j < n; j++) {
      for (int i = 0; i < n; i++) {
        int id = i+j*n;
        unsigned char c;
        c = x[id]*255;
        img.setPixel(i, j, qRgb(0,c,c));
     }
    }
    img.save(filename);
    //img.mirrored(false,true).save(filename);
}


using namespace std::chrono;
class MatrixReplacement;
class HJacReplacement;
using Eigen::SparseMatrix;

namespace Eigen {
namespace internal {
  // MatrixReplacement looks-like a SparseMatrix, so let's inherits its traits:
  template<>
  struct traits<MatrixReplacement> :  public Eigen::internal::traits<Eigen::SparseMatrix<double> >
  {};
  template<>
  struct traits<HJacReplacement> :  public Eigen::internal::traits<Eigen::SparseMatrix<double> >
  {};

}
}


// Example of a matrix-free wrapper from a user type to Eigen's compatible type
class MatrixReplacement : public Eigen::EigenBase<MatrixReplacement> {
private:
    int N;
    double h;
    Eigen::Map<const Eigen::MatrixXd> U;
    double C;

public:

  MatrixReplacement(int N, double h, Eigen::VectorXd & u, double C)
      : N(N), h(h), U(u.data(), N, N), C(C)
  {}
  // Required typedefs, constants, and method:
  typedef double Scalar;
  typedef double RealScalar;
  typedef int StorageIndex;
  enum {
    ColsAtCompileTime = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    IsRowMajor = false
  };
  Index rows() const { return N*N; }
  Index cols() const { return N*N; }
  template<typename Rhs>
  Eigen::Product<MatrixReplacement,Rhs,Eigen::AliasFreeProduct> operator*(const Eigen::MatrixBase<Rhs>& x) const {
    return Eigen::Product<MatrixReplacement,Rhs,Eigen::AliasFreeProduct>(*this, x.derived());
  }
  // Custom API:
  //MatrixReplacement() : N(0), h(0) {}
  //MatrixReplacement(int N, double h) : N(N), h(h) {}
  int get_N() const
  {
      return N;
  }
  double a(int i, int j) const
  {
      return -1.0/(h*h);
  }
  double b(int i, int j) const
  {
      return C/(2*h)*((i == N ? 0 : U(i, j - 1)) + (j == N ? 0 : U(i - 1, j)) - (i == 1 ? 0 : U(i - 2, j - 1)) - (j == 1 ? 0 : U(i - 1, j - 2)));
  }
  double c(int i, int j) const
  {
      return C/(2*h)*U(i-1, j-1);
  }
};


// Example of a matrix-free wrapper from a user type to Eigen's compatible type
class HJacReplacement : public Eigen::EigenBase<HJacReplacement> {
private:
    int N;
    double h;
    Eigen::VectorXd & U;
    double C;

public:

  HJacReplacement(int N, Eigen::VectorXd & u, double C)
      : N(N), h(1.0/N), U(u), C(C)
  {}
  // Required typedefs, constants, and method:
  typedef double Scalar;
  typedef double RealScalar;
  typedef int StorageIndex;
  enum {
    ColsAtCompileTime = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    IsRowMajor = false
  };
  Index rows() const { return N; }
  Index cols() const { return N; }
  template<typename Rhs>
  Eigen::Product<HJacReplacement,Rhs,Eigen::AliasFreeProduct> operator*(const Eigen::MatrixBase<Rhs>& x) const {
    return Eigen::Product<HJacReplacement,Rhs,Eigen::AliasFreeProduct>(*this, x.derived());
  }
  // Custom API:

  int get_N() const
  {
      return N;
  }
  double get_C() const
  {
      return C;
  }
  double a(int i) const
  {
      double tmp = 0;
      double v = h*(i-0.5);
      for (int j = 1; j < N+1; j++) {
          tmp += U(j-1)/(v + h*(j-0.5));
      }
      return 1-C*h/2*v*tmp;
  }
};



// Implementation of MatrixReplacement * Eigen::DenseVector though a specialization of internal::generic_product_impl:
namespace Eigen {
namespace internal {
  template<typename Rhs>
  struct generic_product_impl<MatrixReplacement, Rhs, SparseShape, DenseShape, GemvProduct> // GEMV stands for matrix-vector
  : generic_product_impl_base<MatrixReplacement,Rhs,generic_product_impl<MatrixReplacement,Rhs> >
  {
    typedef typename Product<MatrixReplacement,Rhs>::Scalar Scalar;
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const MatrixReplacement& lhs, const Rhs& rhs, const Scalar& alpha)
    {
      // This method should implement "dst += alpha * lhs * rhs" inplace,
      // however, for iterative solvers, alpha is always equal to 1, so let's not bother about it.
      assert(alpha==Scalar(1) && "scaling is not implemented");
      EIGEN_ONLY_USED_FOR_DEBUG(alpha);

      //come back to matrix representation
      const int N = lhs.get_N();
      Eigen::Map<const Eigen::MatrixXd> U(rhs.data(), N, N);
      Eigen::Map<Eigen::MatrixXd> D(dst.data(), N, N);
      for (int i = 1; i < N+1; i++) {
          for (int j = 1; j < N+1; j++) {
              D(i-1, j-1) = lhs.a(i, j)*(-4*U(i-1, j-1) + (i == N ? 0 : U(i, j-1)) + (i == 1 ? 0 : U(i-2, j-1)) + (j == N ? 0: U(i-1, j)) +
                                                                                               (j == 1 ? 0 : U(i-1, j-2)))
                      + lhs.b(i, j) * U(i-1, j-1) +
                      lhs.c(i, j) * ( (i == N ? 0 : U(i, j-1)) + (j == N ? 0 : U(i-1, j)) - (i == 1 ? 0 : U(i-2, j-1)) - (j == 1 ? 0 : U(i-1, j-2)) );
          }
      }

    }
  };

  template<typename Rhs>
  struct generic_product_impl<HJacReplacement, Rhs, SparseShape, DenseShape, GemvProduct> // GEMV stands for matrix-vector
  : generic_product_impl_base<HJacReplacement,Rhs,generic_product_impl<HJacReplacement,Rhs> >
  {
    typedef typename Product<HJacReplacement,Rhs>::Scalar Scalar;
    template<typename Dest>
    static void scaleAndAddTo(Dest& dst, const HJacReplacement& lhs, const Rhs& rhs, const Scalar& alpha)
    {
      // This method should implement "dst += alpha * lhs * rhs" inplace,
      // however, for iterative solvers, alpha is always equal to 1, so let's not bother about it.
      assert(alpha==Scalar(1) && "scaling is not implemented");
      EIGEN_ONLY_USED_FOR_DEBUG(alpha);

      //come back to matrix representation
      const int N = lhs.get_N();
      const double C = lhs.get_C();
      for (int i = 1; i < N+1; i++) {
        const double A = lhs.a(i);
        const double v = (i-0.5)/N;
        double tmp = 0;
        for (int j = 1; j < N+1; j++) {
            tmp += rhs(j-1)/(v + (j-0.5)/N);
        }
        dst(i-1) = rhs(i-1) - C/2/N/(A*A)*v*tmp;
      }

    }
  };

}
}


template <typename _Scalar>
class FFTWPreconditioner
{
    int N;
    double h;
    double *b;//big buf

    double alphad(int i, int j) const
    {
        return  -cos(i*h)/(2*h*h);
    }

    Eigen::Index row, col;
    typedef _Scalar Scalar;
    typedef Eigen::Matrix<Scalar,Eigen::Dynamic,1> Vector;
  public:

    ~FFTWPreconditioner()
    {
        //fftw_free(s);
        fftw_free(b);
    }
    typedef typename Vector::StorageIndex StorageIndex;
    enum {
      ColsAtCompileTime = Eigen::Dynamic,
      MaxColsAtCompileTime = Eigen::Dynamic
    };

    FFTWPreconditioner() : m_isInitialized(false), b(0) {}

    template<typename MatType>
    explicit FFTWPreconditioner(const MatType& mat) : row(mat.cols()), col(row),
        N(sqrt(row)), h(1.0/(N+1))
    {
       compute(mat);
    }

    Eigen::Index rows() const { return row; }
    Eigen::Index cols() const { return col; }

    template<typename MatType>
    FFTWPreconditioner& analyzePattern(const MatType& )
    {
      return *this;
    }

    template<typename MatType>
    FFTWPreconditioner& factorize(const MatType& mat)
    {
        col = row = mat.cols();
        N = sqrt(row);
        h = 1.0/(N+1);

        b = fftw_alloc_real(N*N);

       m_isInitialized = true;
       return *this;
    }

    template<typename MatType>
    FFTWPreconditioner& compute(const MatType& mat)
    {
      return factorize(mat);
    }

    template<typename Rhs, typename Dest>
    void _solve_impl(const Rhs& from, Dest& to) const
    {
        for (int i = 0; i < N*N; i++)
            b[i] = from(i);
        fftw_plan plan;

        plan = fftw_plan_r2r_2d(N, N, b, b, FFTW_RODFT00, FFTW_RODFT00, FFTW_ESTIMATE);
        fftw_execute ( plan );


        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                double d = 4.0 * (pow(sin(M_PI * (i+1) / (2*(N+1) )), 2) + pow(sin(M_PI * (j+1) / (2*(N+1) )), 2)) / (h*h);
                b[i+j*N] /= d;
            }
        }


        plan = fftw_plan_r2r_2d(N, N, b, b, FFTW_RODFT00, FFTW_RODFT00, FFTW_ESTIMATE);
        fftw_execute ( plan );

        //save
        for (int i = 0; i < N*N; i++)
            to(i) = b[i] / pow((2*(N+1)), 2) ;//normalize if you wish


    }

    template<typename Rhs> inline const Eigen::Solve<FFTWPreconditioner, Rhs>
    solve(const Eigen::MatrixBase<Rhs>& from) const
    {
      eigen_assert(m_isInitialized && "CustomPreconditioner is not initialized.");
      return Eigen::Solve<FFTWPreconditioner, Rhs>(*this, from.derived());
    }

    Eigen::ComputationInfo info() { return Eigen::Success; }

  protected:
    bool m_isInitialized;
};


class Task
{
public:

    int N; //mesh NxN
    double h;
    double C;
    Eigen::VectorXd x;
    MatrixReplacement Jac;

    double boundary(int i, int j)
    {
        return 0;
    }
    double rhs(int i, int j) const
    {
        const double x = i*h;
        const double y = j*h;
        //return 2*(y*(1-y) + x*(1-x)) + C*x*y*(1-x)*(1-y)*((1-2*x)*y*(1-y) + (1-2*y)*x*(1-x));
        return -(10*y*(1-y)*exp(pow(x, 4.5))*(4.5*pow(x,3.5)*(1-2*x+4.5*pow(x, 4.5)*(1-x)) - 2 + pow(4.5,2)*pow(x,3.5) - 4.5*5.5*pow(x, 4.5)) )
                + 20*x*(1-x)*exp(pow(x, 4.5))
                +
                C*10*x*y*(1-x)*(1-y)*exp(pow(x, 4.5))*(10*exp(pow(x, 4.5))*y*(1-y)*(1-2*x + 4.5*pow(x,4.5)*(1-x)) + 10*x*(1-x)*exp(pow(x, 4.5))*(1-2*y));
    }
    Eigen::VectorXd exact_sol()
    {
        Eigen::VectorXd exact_x(N*N);
        Eigen::Map<Eigen::MatrixXd> E(exact_x.data(), N, N);

        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < N+1; j++) {
                const double x = i*h;
                const double y = j*h;
                //E(i-1, j-1) = x*y*(1-x)*(1-y);
                E(i-1, j-1) = 10*x*y*(1-x)*(1-y)*exp(pow(x, 4.5));
            }
        }
        return exact_x;
    }


    Eigen::VectorXd F(Eigen::VectorXd &u)
    {
        Eigen::VectorXd ff(N*N);
        Eigen::Map<Eigen::MatrixXd> f(ff.data(), N, N);
        Eigen::Map<Eigen::MatrixXd> U(u.data(), N, N);

        for (int i = 1; i < N+1; i++) {
            for (int j = 1; j < N+1; j++) {
                f(i-1, j-1) = -1/(h*h) * (-4*U(i-1, j-1) + (i == N ? 0 : U(i, j-1)) + (i == 1 ? 0 : U(i-2, j-1)) + (j == N ? 0: U(i-1, j)) +
                                (j == 1 ? 0 : U(i-1, j-2)) ) +

                        C*U(i-1, j-1)/(2*h)*( (i == N ? 0 : U(i, j-1)) + (j == N ? 0 : U(i-1, j)) - (i == 1 ? 0 : U(i-2, j-1)) - (j == 1 ? 0 : U(i-1, j-2)) )
                        - rhs(i, j);
            }
        }

        return ff;
    }

    Task(int N, double C)
        : N(N), h(1.0/(N+1)), C(C), x(N*N), Jac(N, h, x, C)
    {}


typedef enum {bicgstab, gmres} solver;
    void solve(solver st, QLineSeries & series, double & time, bool deb = 0)
    {
        x.setZero();
        double eps_o = 1e-5, eps_i = 1e-5;
        Eigen::VectorXd z(N*N), temp(N*N);
        double znorm = 1;
        double tnorm;
        double bn = tnorm = F(x).lpNorm<Eigen::Infinity>();
        int max_o_it = 100;
        high_resolution_clock::time_point t1 = high_resolution_clock::now();

        int i = 0;
        for (i = 0; i < max_o_it && znorm > eps_o; i++) {
            if (st == bicgstab) {
              //Eigen::BiCGSTAB<MatrixReplacement, Eigen::IdentityPreconditioner> bicg;

              Eigen::BiCGSTAB<MatrixReplacement, FFTWPreconditioner<double>> bicg;
              bicg.compute(Jac);
              bicg.setTolerance(eps_i);
              temp = F(x);
              tnorm = temp.lpNorm<Eigen::Infinity>();
              z = bicg.solve(temp);
              if (deb)
                std::cout << "BiCGSTAB: #iterations: " << bicg.iterations() << ", estimated error: " << bicg.error() << std::endl;
              //double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
              //std::cout << "Eigen_BiCGSTAB Error: " <<  err << std::endl;
             } else {
                  Eigen::GMRES<MatrixReplacement, FFTWPreconditioner<double>> gmres;
                  gmres.compute(Jac);
                  gmres.setTolerance(eps_i);
                  temp = F(x);
                  tnorm = temp.lpNorm<Eigen::Infinity>();
                  z = gmres.solve(temp);
                  if (deb)
                    std::cout << "GMRES:    #iterations: " << gmres.iterations() << ", estimated error: " << gmres.error() << std::endl;
                    //double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
                    //std::cout << "Eigen_GMRES Error: " <<  err << std::endl;
            }
            x -= z;
            znorm = z.lpNorm<Eigen::Infinity>();
            series.append(i, tnorm/bn);
        }
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        time = duration_cast<milliseconds>( t2 - t1 ).count();
        series.append(i, F(x).lpNorm<Eigen::Infinity>()/bn);
        if (deb) {
          std::cout << "Outer iters: " << i << std::endl;
          std::cout << "Relative res: " << F(x).lpNorm<Eigen::Infinity>()/bn << std::endl;
          double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
          std::cout << "Error: " <<  err << std::endl;
        }
    }
};



class TaskH
{
public:

    int N;
    double h;
    double C;
    Eigen::VectorXd x;
    HJacReplacement Jac;

    Eigen::VectorXd F(Eigen::VectorXd &u)
    {
        Eigen::VectorXd f(N);
        for (int i = 1; i < N+1; i++) {
            const double v = (i - 0.5)/N;
            double tmp = 0;
            for (int j = 1; j < N+1; j++) {
                tmp += u(j-1)/(v + (j-0.5)/N);
            }
            tmp = 1 - C*v/2/N*tmp;
            f(i-1) = u(i-1) - 1.0/tmp;
        }
        return f;
    }

    TaskH(int N, double C)
        : N(N), h(1.0/N), C(C), x(N), Jac(N, x, C)
    {}


typedef enum {bicgstab, gmres} solver;
    void solve(solver st, QLineSeries & series, double & time, bool deb = 0)
    {
        x.setZero();
        //x.setConstant(1);
        double eps_o = 1e-5, eps_i = 1e-5;
        Eigen::VectorXd z(N), temp(N);
        double znorm = 1;
        double tnorm;
        double bn = tnorm = F(x).lpNorm<Eigen::Infinity>();
        int max_o_it = 100;
        high_resolution_clock::time_point t1 = high_resolution_clock::now();

        int i = 0;
        for (i = 0; i < max_o_it && znorm > eps_o; i++) {
            if (st == bicgstab) {
              //Eigen::BiCGSTAB<MatrixReplacement, Eigen::IdentityPreconditioner> bicg;

              Eigen::BiCGSTAB<HJacReplacement, Eigen::IdentityPreconditioner> bicg;
              bicg.compute(Jac);
              bicg.setTolerance(eps_i);
              temp = F(x);
              tnorm = temp.lpNorm<Eigen::Infinity>();
              z = bicg.solve(temp);
              if (deb)
                std::cout << "BiCGSTAB: #iterations: " << bicg.iterations() << ", estimated error: " << bicg.error() << std::endl;
              //double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
              //std::cout << "Eigen_BiCGSTAB Error: " <<  err << std::endl;
             } else {
                  Eigen::GMRES<HJacReplacement, Eigen::IdentityPreconditioner> gmres;
                  gmres.compute(Jac);
                  gmres.setTolerance(eps_i);
                  temp = F(x);
                  tnorm = temp.lpNorm<Eigen::Infinity>();
                  z = gmres.solve(temp);
                  if (deb)
                    std::cout << "GMRES:    #iterations: " << gmres.iterations() << ", estimated error: " << gmres.error() << std::endl;
                    //double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
                    //std::cout << "Eigen_GMRES Error: " <<  err << std::endl;
            }
            x -= z;
            znorm = z.lpNorm<Eigen::Infinity>();
            series.append(i, tnorm/bn);
        }
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        time = duration_cast<milliseconds>( t2 - t1 ).count();
        series.append(i, F(x).lpNorm<Eigen::Infinity>()/bn);
        if (deb) {
          std::cout << "Outer iters: " << i << std::endl;
          std::cout << "Relative res: " << F(x).lpNorm<Eigen::Infinity>()/bn << std::endl;
         // double err = (x - exact_sol()).lpNorm<Eigen::Infinity>();
          //std::cout << "Error: " <<  err << std::endl;
        }
    }
};

template <typename T>
void benchmark(double C)
{
    std::vector<int> N = {32, 64, 128};
    int tn = 5;
    double time;
    for (auto &n : N) {
        T task(n, C);
        std::cout << "Task of order " << n << std::endl;
        //GMRES
        time = 0;
        for (int i = 0; i < tn; i++) {
            QLineSeries series;
            double tt;
            task.solve(T::gmres, series, tt);
            time+=tt;
        }
        time /= tn;
        std::cout << "GMRES: " << "t(ms): " << time << std::endl;
        //BiCGSTAB
        time = 0;
        for (int i = 0; i < tn; i++) {
            QLineSeries series;
            double tt;
            task.solve(T::bicgstab, series, tt);
            time+=tt;
         }
         time /= tn;
         std::cout << " BiCGSTAB: " << "t(ms): " << time << std::endl;
    }
}

void display_h_solution(QApplication & app, Eigen::VectorXd & x)
{
    QLineSeries h;
    int N = x.size();
    for (int i = 0; i < N; i++)
        h.append(1.0*i/(N-1), x(i));

    QChart *chart = new QChart();
    chart->addSeries(&h);

    //chart->legend()->hide();
    chart->setTitle(QStringLiteral("Solution of H-equation, mesh %1").arg(N) );
    //chart->createDefaultAxes();

    QValueAxis *axisX = new QValueAxis();
    axisX->setTitleText("x");
    axisX->setLabelFormat("%g");
    //axisX->setTickCount(7);
    axisX->setMax(1);
    axisX->setMin(0);
    chart->addAxis(axisX, Qt::AlignBottom);
    h.attachAxis(axisX);


    QValueAxis *axisY = new QValueAxis();

    axisY->setTitleText("H(x)");
    axisY->setLabelFormat("%g");
    axisY->setMax(3);
    axisY->setMin(1);
    axisY->setMinorTickCount(-1);
    chart->addAxis(axisY, Qt::AlignLeft);
    h.attachAxis(axisY);


    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QMainWindow window;
    window.setCentralWidget(chartView);
    window.resize(800, 600);
    window.show();
    app.exec();
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QLineSeries lol;
    double t;
    int N = 128;

    TaskH taskh(N, 0.9999);
    taskh.solve(TaskH::gmres, lol, t, 1);
    //std::cout << taskh.x << std::endl;
    display_h_solution(app, taskh.x);
    std::cout << "timeH_gmres: " << t << std::endl;
    taskh.solve(TaskH::bicgstab, lol, t, 1);
    std::cout << "timeH_bicg: " << t << std::endl;

    QLineSeries ser;

    Task task(N, 20);
    task.solve(Task::gmres, ser, t, 1);
    std::cout << "time_gmres: " << t << std::endl;
    task.solve(Task::bicgstab, ser, t, 1);
    std::cout << "time_bicg: " << t << std::endl;
   /*
    std::cout << "***BENCHMARK H-EQUATION***" << std::endl;
    benchmark<TaskH>(0.9);

    std::cout << "***BENCHMARK CONVECTION-DIFFUSION***" << std::endl;
    benchmark<Task>(20);

    QLineSeries bicgstab;
    bicgstab.setName("BiCGSTAB");
    task.solve(Task::bicgstab, bicgstab, t, 1);
//    saveAsBitmap((task.x - task.exact_sol()).cwiseAbs(), N, "error.bmp");
   // saveAsBitmap(task.x, N, "appr_solution.bmp");
   // saveAsBitmap(task.exact_sol(), N, "exect_solution.bmp");

    QLineSeries gmres;
    gmres.setName("GMRES");
    task.solve(Task::gmres, gmres, t, 1);

    QChart *chart = new QChart();
   chart->addSeries(&bicgstab);
    chart->addSeries(&gmres);

    //chart->legend()->hide();
    chart->setTitle(QStringLiteral("Convection-diffusion, mesh %1x%1").arg(N) );

    QValueAxis *axisX = new QValueAxis();
    axisX->setTitleText("Outer iterations");
    axisX->setLabelFormat("%i");
    axisX->setTickCount(7);
    axisX->setMax(6);
    chart->addAxis(axisX, Qt::AlignBottom);
    gmres.attachAxis(axisX);
    bicgstab.attachAxis(axisX);


    QLogValueAxis *axisY = new QLogValueAxis();
    axisY->setTitleText("Relative residual");
    axisY->setLabelFormat("%g");
    axisY->setBase(8.0);
    axisY->setMax(1);
    axisY->setMin(bicgstab.at(bicgstab.count()-1).y()/2);
    axisY->setMinorTickCount(-1);
    chart->addAxis(axisY, Qt::AlignLeft);
    bicgstab.attachAxis(axisY);
    gmres.attachAxis(axisY);


    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QMainWindow window;
    window.setCentralWidget(chartView);
    window.resize(800, 600);
    window.show();
*/
    return app.exec();
}
